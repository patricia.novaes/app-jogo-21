package br.com.itau;
import java.util.List;

public class AppJogo21 {

    public static void main(String[] args) {
        Jogo jogo = new Jogo();

        List<Carta> cartasPrimeiraRodada = jogo.jogarPrimeiraRodada();
        Boolean querJogar = true;

        Impressora.imprimirJogada(cartasPrimeiraRodada, jogo);

        querJogar = Impressora.querJogar();

        while(!jogo.estaEncerrado() && querJogar) {
            Carta carta = jogo.jogar();

            Impressora.imprimirJogada(carta, jogo);

            if(!jogo.estaEncerrado()){
                querJogar = Impressora.querJogar();
            }
        }

        Impressora.imprimirJogoFinalizado(jogo);
    }
}


