package br.com.itau;
import java.util.ArrayList;
import java.util.List;

public class Baralho {

    private List<Carta> cartas = new ArrayList<>();

    public Baralho() {
        for(Naipe naipe : Naipe.values()) {
            for(Pontuacao pontuacao : Pontuacao.values()) {
                Carta carta = new Carta(naipe, pontuacao);
                cartas.add(carta);
            }
        }
    }

    public Carta sortear() {
        int posicao = (int) (Math.random() * cartas.size());

        return cartas.remove(posicao);
    }

    public List<Carta> sortear(int quantidade){
        List<Carta> cartas = new ArrayList<>();

        for(int i = 0; i < quantidade; i++) {
            cartas.add(sortear());
        }

        return cartas;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for(Carta carta : cartas) {
            builder.append(carta);
            builder.append("\n");
        }

        return builder.toString();
    }
}