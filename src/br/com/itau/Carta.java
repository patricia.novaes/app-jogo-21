package br.com.itau;


public class Carta {
    private Naipe naipe;
    private Pontuacao pontuacao;

    public Carta(Naipe naipe, Pontuacao pontuacao) {
        this.naipe = naipe;
        this.pontuacao = pontuacao;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public Pontuacao getPontuacao() {
        return pontuacao;
    }

    @Override
    public String toString() {
        return pontuacao + " de " + naipe;
    }
}

