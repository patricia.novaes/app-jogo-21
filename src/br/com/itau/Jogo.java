package br.com.itau;

import java.util.List;

public class Jogo {

    private Baralho baralho = new Baralho();
    private int pontuacao = 0;
    private boolean estaEncerrado = false;
    private boolean vitoria = false;

    public List<Carta> jogarPrimeiraRodada() {
        List<Carta> cartas = baralho.sortear(2);

        for(Carta carta : cartas) {
            pontuacao += carta.getPontuacao().getPontuacao();
        }

        return cartas;
    }

    public Carta jogar() {
        Carta carta = baralho.sortear();

        pontuacao += carta.getPontuacao().getPontuacao();

        verificarFinalizacao();

        return carta;
    }

    private void verificarFinalizacao() {
        if(pontuacao >= 21) {
            estaEncerrado = true;
        }
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public boolean estaEncerrado() {
        return estaEncerrado;
    }

    public boolean vitoria() {
        return vitoria;
    }

}
