package br.com.itau;

public enum Naipe {
    COPAS,
    PAUS,
    OUROS,
    ESPADAS;
}
